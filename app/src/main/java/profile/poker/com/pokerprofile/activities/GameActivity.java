package profile.poker.com.pokerprofile.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.MutableInt;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.Random;

import profile.poker.com.pokerprofile.R;
import profile.poker.com.pokerprofile.utils.Cards;
import profile.poker.com.pokerprofile.utils.Constants;

/**
 * Created by espin on 12/9/2017.
 */

public class GameActivity extends AppCompatActivity {

    String userId;
    int aiScoreCount;
    int playerScoreCount;

    TextView aiScore;
    TextView playerScore;
    ImageView aiCard1;
    ImageView aiCard2;
    ImageView playerCard1;
    ImageView playerCard2;
    ImageView deck;
    Button newGame;
    Button drawCards;
    Button exitGame;

    LinearLayout btnGroup;

    Deque<Cards> cardDeck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        init();
        initListener();
        initCardDeck();
    }

    private void init(){

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        userId = getIntent().getStringExtra(Constants.USER_ID_LITERAL.toString());

        aiScoreCount = 0;
        playerScoreCount = 0;

        aiScore = (TextView) findViewById(R.id.ai_score);
        playerScore = (TextView) findViewById(R.id.player_score);
        aiCard1 = (ImageView) findViewById(R.id.ai_card_1);
        aiCard2 = (ImageView) findViewById(R.id.ai_card_2);
        playerCard1 = (ImageView) findViewById(R.id.player_card_1);
        playerCard2 = (ImageView) findViewById(R.id.player_card_2);
        deck = (ImageView) findViewById(R.id.deck);
        newGame = (Button) findViewById(R.id.btn_new_game);
        drawCards = (Button) findViewById(R.id.btn_draw);
        exitGame = (Button) findViewById(R.id.btn_exit_game);
        btnGroup = (LinearLayout) findViewById(R.id.game_button_group);

        ViewGroup.LayoutParams btnGrpParams = btnGroup.getLayoutParams();
        btnGrpParams.width = displayMetrics.widthPixels / 2;
        btnGroup.setLayoutParams(btnGrpParams);

        updateScoreText();

    }
    private void updateScoreText(){
        aiScore.setText(Constants.SCORE_WITH_COLON_LITERAL.toString() + aiScoreCount);
        playerScore.setText(Constants.SCORE_WITH_COLON_LITERAL.toString() + playerScoreCount);
    }
    private void initCardDeck(){
        List<Cards> cards = new ArrayList<Cards>(Arrays.asList(
                Cards.c1,Cards.c2,Cards.c3,Cards.c4,Cards.c5,Cards.c6,Cards.c7,Cards.c8,Cards.c9,Cards.c10,Cards.c11,Cards.c12,Cards.c13,
                Cards.d1,Cards.d2,Cards.d3,Cards.d4,Cards.d5,Cards.d6,Cards.d7,Cards.d8,Cards.d9,Cards.d10,Cards.d11,Cards.d12,Cards.d13,
                Cards.h1,Cards.h2,Cards.h3,Cards.h4,Cards.h5,Cards.h6,Cards.h7,Cards.h8,Cards.h9,Cards.h10,Cards.h11,Cards.h12,Cards.h13,
                Cards.s1,Cards.s2,Cards.s3,Cards.s4,Cards.s5,Cards.s6,Cards.s7,Cards.s8,Cards.s9,Cards.s10,Cards.s11,Cards.s12,Cards.s13
                ));


        cardDeck = shuffle(cards);
    }
    private void initListener(){
        newGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initCardDeck();
                drawCards.setEnabled(true);
                newGame.setEnabled(false);
                aiScoreCount = 0;
                playerScoreCount = 0;
                aiCard1.setImageResource(R.drawable.card_back);
                aiCard2.setImageResource(R.drawable.card_back);
                playerCard1.setImageResource(R.drawable.card_back);
                playerCard2.setImageResource(R.drawable.card_back);

                updateScoreText();
            }
        });

        drawCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MutableInt aiPoint = new MutableInt(0);
                MutableInt playerPoint = new MutableInt(0);

                aiCard1.setImageResource(getDrawable(cardDeck.pop().toString(), aiPoint));
                playerCard1.setImageResource(getDrawable(cardDeck.pop().toString(), playerPoint));
                aiCard2.setImageResource(getDrawable(cardDeck.pop().toString(), aiPoint));
                playerCard2.setImageResource(getDrawable(cardDeck.pop().toString(), playerPoint));

                if(aiPoint.value > playerPoint.value){
                    aiScoreCount ++;
                } else if(aiPoint.value < playerPoint.value){
                    playerScoreCount ++;
                }

                updateScoreText();

                if(cardDeck.isEmpty()){
                    String message = "";
                    drawCards.setEnabled(false);
                    newGame.setEnabled(true);
                    if(playerScoreCount > aiScoreCount){
                        message = "Congratulations! You won!";
                    } else if (playerScoreCount < aiScoreCount){
                        message = "Sorry, you lost. Better luck next time.";
                    } else {
                        message = "It's a draw.";
                    }
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                }

            }
        });

        exitGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()) {
            case R.id.home: // back to previous activity
                onBackPressed();
                return true;
            default:
                onBackPressed();
                return true;
        }
//        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.putExtra(Constants.USER_ID_LITERAL.toString(), userId);
        startActivity(intent);
        finish();
    }

    private Deque<Cards> shuffle(List<Cards> array) {

        Collections.shuffle(array);

        return new ArrayDeque<>(array);
    }

    private int getDrawable(String c, MutableInt point){
        int drawable = 0;

        switch (c){
            case "c1": drawable = R.drawable.c1; point.value += 1; break;
            case "c2": drawable = R.drawable.c2; point.value += 2; break;
            case "c3": drawable = R.drawable.c3; point.value += 3; break;
            case "c4": drawable = R.drawable.c4; point.value += 4; break;
            case "c5": drawable = R.drawable.c5; point.value += 5; break;
            case "c6": drawable = R.drawable.c6; point.value += 6; break;
            case "c7": drawable = R.drawable.c7; point.value += 7; break;
            case "c8": drawable = R.drawable.c8; point.value += 8; break;
            case "c9": drawable = R.drawable.c9; point.value += 9; break;
            case "c10": drawable = R.drawable.c10; point.value += 10; break;
            case "c11": drawable = R.drawable.c11; point.value += 11; break;
            case "c12": drawable = R.drawable.c12; point.value += 12; break;
            case "c13": drawable = R.drawable.c13; point.value += 13; break;
            case "d1": drawable = R.drawable.d1; point.value += 1; break;
            case "d2": drawable = R.drawable.d2; point.value += 2; break;
            case "d3": drawable = R.drawable.d3; point.value += 3; break;
            case "d4": drawable = R.drawable.d4; point.value += 4; break;
            case "d5": drawable = R.drawable.d5; point.value += 5; break;
            case "d6": drawable = R.drawable.d6; point.value += 6; break;
            case "d7": drawable = R.drawable.d7; point.value += 7; break;
            case "d8": drawable = R.drawable.d8; point.value += 8; break;
            case "d9": drawable = R.drawable.d9; point.value += 9; break;
            case "d10": drawable = R.drawable.d10; point.value += 10; break;
            case "d11": drawable = R.drawable.d11; point.value += 11; break;
            case "d12": drawable = R.drawable.d12; point.value += 12; break;
            case "d13": drawable = R.drawable.d13; point.value += 13; break;
            case "h1": drawable = R.drawable.h1; point.value += 1; break;
            case "h2": drawable = R.drawable.h2; point.value += 2; break;
            case "h3": drawable = R.drawable.h3; point.value += 3; break;
            case "h4": drawable = R.drawable.h4; point.value += 4; break;
            case "h5": drawable = R.drawable.h5; point.value += 5; break;
            case "h6": drawable = R.drawable.h6; point.value += 6; break;
            case "h7": drawable = R.drawable.h7; point.value += 7; break;
            case "h8": drawable = R.drawable.h8; point.value += 8; break;
            case "h9": drawable = R.drawable.h9; point.value += 9; break;
            case "h10": drawable = R.drawable.h10; point.value += 10; break;
            case "h11": drawable = R.drawable.h11; point.value += 11; break;
            case "h12": drawable = R.drawable.h12; point.value += 12; break;
            case "h13": drawable = R.drawable.h13; point.value += 13; break;
            case "s1": drawable = R.drawable.s1; point.value += 1; break;
            case "s2": drawable = R.drawable.s2; point.value += 2; break;
            case "s3": drawable = R.drawable.s3; point.value += 3; break;
            case "s4": drawable = R.drawable.s4; point.value += 4; break;
            case "s5": drawable = R.drawable.s5; point.value += 5; break;
            case "s6": drawable = R.drawable.s6; point.value += 6; break;
            case "s7": drawable = R.drawable.s7; point.value += 7; break;
            case "s8": drawable = R.drawable.s8; point.value += 8; break;
            case "s9": drawable = R.drawable.s9; point.value += 9; break;
            case "s10": drawable = R.drawable.s10; point.value += 10; break;
            case "s11": drawable = R.drawable.s11; point.value += 11; break;
            case "s12": drawable = R.drawable.s12; point.value += 12; break;
            case "s13": drawable = R.drawable.s13; point.value += 13; break;
        }

        return drawable;
    }
}
