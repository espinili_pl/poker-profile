package profile.poker.com.pokerprofile.utils;

/**
 * Created by espin on 10/9/2017.
 */

public class Constants {

    public static final String USERNAME_LITERAL = "username";

    public static final String PASSWORD_LITERAL = "password";

    public static final String FIRST_NAME_LITERAL = "firstName";

    public static final String LAST_NAME_LITERAL = "lastName";

    public static final String USERS_LITERAL = "users";

    public static final String USER_LITERAL = "user";

    public static final String USER_ID_LITERAL = "userId";

    public static final String SCORE_WITH_COLON_LITERAL= "Score : ";

}
