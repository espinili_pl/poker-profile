package profile.poker.com.pokerprofile.activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;

import profile.poker.com.pokerprofile.R;
import profile.poker.com.pokerprofile.entities.User;
import profile.poker.com.pokerprofile.utils.Constants;
import profile.poker.com.pokerprofile.utils.FirebaseDBUtil;

public class MainActivity extends AppCompatActivity {

    Context context;
    EditText usernameField;
    EditText passwordField;

    private TaskCompletionSource<Object> source;
    private Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = getApplicationContext();
        usernameField = (EditText) findViewById(R.id.txt_email);
        passwordField = (EditText) findViewById(R.id.txt_password);
    }

    public void registerAccount (View v) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);

    }

    public void signIn (View v) {
        FirebaseDBUtil dbUtil = new FirebaseDBUtil();
        source = new TaskCompletionSource<>();
        dbUtil.login(usernameField.getText().toString(), passwordField.getText().toString(), source);
        task = source.getTask();
        task.addOnCompleteListener(MainActivity.this , new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                User user = (User) task.getResult();
                if(user != null && user.getId() != null){
                    Intent intent = new Intent(context, HomeActivity.class);
                    intent.putExtra(Constants.USER_ID_LITERAL.toString(), user.getId());
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Incorrect email or password", Toast.LENGTH_LONG).show();
                    passwordField.setText("");
                }
            }
        });

    }
}
