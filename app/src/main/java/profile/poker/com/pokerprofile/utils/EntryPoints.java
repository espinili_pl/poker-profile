package profile.poker.com.pokerprofile.utils;

/**
 * Created by espin on 11/9/2017.
 */

public enum EntryPoints {
    LOGIN, EMAIL_VALIDATION, GET_USER
}
