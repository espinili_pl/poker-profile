package profile.poker.com.pokerprofile.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;

import org.w3c.dom.Text;

import profile.poker.com.pokerprofile.R;
import profile.poker.com.pokerprofile.entities.User;
import profile.poker.com.pokerprofile.utils.Constants;
import profile.poker.com.pokerprofile.utils.FirebaseDBUtil;

/**
 * Created by espin on 10/9/2017.
 */

public class HomeActivity extends AppCompatActivity {

    private final User user = new User();
    private String userId;

    private String username;
    private String password;

    private TextView nameField;
    private TextView emailField;
    private ImageView edit;
    private Button startGame;

    FirebaseDBUtil dbUtil = new FirebaseDBUtil();
    TaskCompletionSource<Object> source = new TaskCompletionSource<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        init();
    }

    private void init(){
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);

        userId = getIntent().getStringExtra(Constants.USER_ID_LITERAL.toString());
        if(null == userId || "" == userId){
            username = getIntent().getStringExtra(Constants.USERNAME_LITERAL.toString());
            password = getIntent().getStringExtra(Constants.PASSWORD_LITERAL.toString());
        }

        initializeUser();

        edit = (ImageView) findViewById(R.id.icon_edit);
        startGame = (Button) findViewById(R.id.btn_start_game);
        nameField = (TextView)findViewById(R.id.user_profile_name);
        emailField= (TextView)findViewById(R.id.user_profile_email);

    }

    private void initializeFields(){
        nameField.setText(user.getFirstName() + " " + user.getLastName());
        emailField.setText(user.getUsername());
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), EditProfileActivity.class);
                intent.putExtra(Constants.USER_ID_LITERAL, user.getId());
                startActivity(intent);
            }
        });

        startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                intent.putExtra(Constants.USER_ID_LITERAL, user.getId());
                startActivity(intent);
                finish();
            }
        });
    }
    private void initializeUser(){
        source = new TaskCompletionSource<>();
        if(userId != null) {
            dbUtil.getUser(userId, source);
        } else {
            dbUtil.login(username, password, source);
        }
        Task task = source.getTask();
        task.addOnCompleteListener(this, new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                User u = (User)task.getResult();
                if(u != null){
                    user.setId(u.getId());
                    user.setUsername(u.getUsername());
                    user.setPassword(u.getPassword());
                    user.setFirstName(u.getFirstName());
                    user.setLastName(u.getLastName());
                    initializeFields();
                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }
    //and this to handle actions
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_sign_out) {
            showLogoutMessage();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        showLogoutMessage();
    }

    private void showLogoutMessage(){
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AppTheme));

        alertBuilder.setTitle("Sign Out");

        alertBuilder
                .setMessage("Are you sure you want to sign out?")
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();
    }
}
