package profile.poker.com.pokerprofile;

/**
 * Created by espin on 10/9/2017.
 */

public interface SimpleCallback<T> {
    void callback(T data);
}
