package profile.poker.com.pokerprofile.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;

import profile.poker.com.pokerprofile.R;
import profile.poker.com.pokerprofile.entities.User;
import profile.poker.com.pokerprofile.utils.Constants;
import profile.poker.com.pokerprofile.utils.FirebaseDBUtil;

/**
 * Created by espin on 11/9/2017.
 */

public class EditProfileActivity extends AppCompatActivity {

    String userId;
    final User user = new User();
    TextView emailField;
    TextView nameField;
    EditText firstNameField;
    EditText lastNameField;
    EditText passwordField;
    EditText confirmPasswordField;
    Button updateButton;

    FirebaseDBUtil dbUtil;
    TaskCompletionSource<Object> source = new TaskCompletionSource<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        init();
        addFieldListeners();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()) {
            case R.id.home: // back to previous activity
                onBackPressed();
                return true;
            default:
                onBackPressed();
                return true;
        }
//        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.putExtra(Constants.USER_ID_LITERAL.toString(), user.getId()
        );
        startActivity(intent);
        finish();
    }

    private void init(){

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        userId = getIntent().getStringExtra(Constants.USER_ID_LITERAL);
        emailField = (TextView) findViewById(R.id.lbl_email);
        nameField = (TextView) findViewById(R.id.lbl_name);
        firstNameField = (EditText) findViewById(R.id.txt_first_name);
        lastNameField = (EditText) findViewById(R.id.txt_last_name);
        passwordField = (EditText) findViewById(R.id.txt_password);
        confirmPasswordField = (EditText) findViewById(R.id.txt_repeat_password);
        updateButton = (Button)findViewById(R.id.btn_save_profile);

        dbUtil = new FirebaseDBUtil();

        source = new TaskCompletionSource<>();
        dbUtil.getUser(userId, source);
        Task task = source.getTask();
        task.addOnCompleteListener(this, new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                User u = (User)task.getResult();
                if(u != null){
                    user.setId(u.getId());
                    user.setUsername(u.getUsername());
                    user.setPassword(u.getPassword());
                    user.setFirstName(u.getFirstName());
                    user.setLastName(u.getLastName());
                    nameField.setText(user.getFirstName() + " " + user.getLastName());
                    emailField.setText(user.getUsername());
                }
            }
        });


    }

    private void addFieldListeners(){
        passwordField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.e("sequence", charSequence.toString());
                if (!charSequence.toString().isEmpty()) {
                    confirmPasswordField.setVisibility(View.VISIBLE);
                } else {
                    confirmPasswordField.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    String firstName = null;
                    String lastName = null;
                    String password = null;

                    if(firstNameField.getText() != null || firstNameField.getText().toString() != ""){
                        firstName = firstNameField.getText().toString();
                    }
                    if(lastNameField.getText() != null || lastNameField.getText().toString() != ""){
                        lastName = lastNameField.getText().toString();
                    }
                    if(passwordField.getText() != null || passwordField.getText().toString() != ""){
                        if(confirmPasswordField.getText() != null || confirmPasswordField.getText().toString() != ""){
                            String pw = passwordField.getText().toString();
                            String confirmPassword = confirmPasswordField.getText().toString();
                            if(pw.equals(confirmPassword)){
                                password = pw;
                            } else {
                                throw new Exception("Passwords did not match");
                            }
                        } else {
                            throw new Exception("Please enter confirm password.");
                        }
                    }

                    if(firstName != null && !firstName.isEmpty()) {
                        user.setFirstName(firstName);
                    }
                    if(lastName != null && !lastName.isEmpty()){
                        user.setLastName(lastName);
                    }
                    if(password != null && !password.isEmpty()){
                        user.setPassword(password);
                    }
                    source = new TaskCompletionSource<Object>();
                    dbUtil.updateProfile(userId,firstName, lastName, password, source);
                    Task task = source.getTask();
                    task.addOnCompleteListener(EditProfileActivity.this, new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            Boolean result = (Boolean)task.getResult();
                            if(result){
                                Toast.makeText(getApplicationContext(), "Profile successfully updated", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                intent.putExtra(Constants.USER_LITERAL.toString(), user);
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), "Something went wrong when updating your profile. Please try again.", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }


}
