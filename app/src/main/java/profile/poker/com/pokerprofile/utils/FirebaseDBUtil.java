package profile.poker.com.pokerprofile.utils;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import profile.poker.com.pokerprofile.SimpleCallback;
import profile.poker.com.pokerprofile.entities.User;
import profile.poker.com.pokerprofile.listeners.DataChangeListener;

/**
 * Created by espin on 10/9/2017.
 */

public class FirebaseDBUtil {

    private FirebaseDatabase database;

    public FirebaseDBUtil(){
        database = FirebaseDatabase.getInstance();
    }

    public void login(final String username, final String password, final TaskCompletionSource<Object> source){

        DatabaseReference ref = getDatabaseReference(Constants.USERS_LITERAL);
        ref.orderByChild(Constants.USERNAME_LITERAL)
                .equalTo(username);
        ref.addValueEventListener(new DataChangeListener(username, password, EntryPoints.LOGIN.toString(),
                new SimpleCallback<User>() {
                    @Override
                    public void callback(User data) {
                        if(!source.getTask().isComplete()) {
                            source.setResult(data);
                        }
                    }
                }));

    }

    public User registerNewAccount(User user){
        User returnUser = user;
        try {
            DatabaseReference ref = getDatabaseReference(Constants.USERS_LITERAL);
            ref.push().setValue(user);
        }catch(Exception e){
                returnUser = null;
        }
        return returnUser;
    }

    public boolean isEmailExisting(String email, final TaskCompletionSource<User> source) {
        boolean result = false;

        DatabaseReference ref = getDatabaseReference(Constants.USERS_LITERAL);
        ref.orderByChild(Constants.USERNAME_LITERAL)
                .equalTo(email);

        ref.addValueEventListener(new DataChangeListener(email, EntryPoints.EMAIL_VALIDATION.toString(),
                new SimpleCallback<User>() {
                    @Override
                    public void callback(User data) {
                        source.setResult(data);
                    }
                }));

        return result;
    }

    public User getUser(final String userId, final TaskCompletionSource<Object> source){
        User result = null;

        DatabaseReference ref = getDatabaseReference(Constants.USERS_LITERAL + "/" + userId);

        ref.addValueEventListener(new DataChangeListener(EntryPoints.GET_USER.toString(),
                new SimpleCallback<User>() {
                    @Override
                    public void callback(User data) {
                        data.setId(userId);
                        if(!source.getTask().isComplete()) {
                            source.setResult(data);
                        }
                    }
                }));

        return result;
    }

    public void updateProfile(String userId, String firstName, String lastName, String password, final TaskCompletionSource<Object> source){
        boolean result = false;
        DatabaseReference ref = getDatabaseReference(Constants.USERS_LITERAL + "/" + userId);

        if(firstName != null && !firstName.isEmpty()) {
            ref.child(Constants.FIRST_NAME_LITERAL.toString()).setValue(firstName);
            result = true;
        }
        if(lastName != null && !lastName.isEmpty()){
            ref.child(Constants.LAST_NAME_LITERAL.toString()).setValue(lastName);
            result = true;
        }
        if(password != null && !password.isEmpty()){
            ref.child(Constants.PASSWORD_LITERAL.toString()).setValue(password);
            result = true;
        }

        source.setResult(result);
    }

    private DatabaseReference getDatabaseReference(String reference){
        Log.e("reference", reference);
        if(reference != null){
            return database.getReference(reference);
        } else {
            return database.getReference();
        }
    }
}
