package profile.poker.com.pokerprofile.entities;

import java.io.Serializable;
import java.util.Map;

import profile.poker.com.pokerprofile.utils.Constants;

/**
 * Created by espin on 10/9/2017.
 */

public class User implements Serializable,Cloneable{

    private String id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;

    public User() {}

    public User(Map<String, String> map) {
        this.setUsername(map.get(Constants.USERNAME_LITERAL));
        this.setPassword(map.get(Constants.PASSWORD_LITERAL));
        this.setFirstName(map.get(Constants.FIRST_NAME_LITERAL));
        this.setLastName(map.get(Constants.LAST_NAME_LITERAL));
    }

    public User(String username, String password, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public User clone(){
        User user = new User();
        user.setId(this.id);
        user.setUsername(this.username);
        user.setPassword(this.password);
        user.setFirstName(this.firstName);
        user.setLastName(this.lastName);
        return user;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
