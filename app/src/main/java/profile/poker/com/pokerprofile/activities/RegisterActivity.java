package profile.poker.com.pokerprofile.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;

import profile.poker.com.pokerprofile.R;
import profile.poker.com.pokerprofile.entities.User;
import profile.poker.com.pokerprofile.utils.Constants;
import profile.poker.com.pokerprofile.utils.FirebaseDBUtil;

/**
 * Created by espin on 10/9/2017.
 */

public class RegisterActivity extends AppCompatActivity {

    Context context;
    EditText usernameField;
    EditText passwordField;
    EditText firstNameField;
    EditText lastNameField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);

        init();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()) {
            case R.id.home: // back to previous activity
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void init(){

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        context = getApplicationContext();

        usernameField = (EditText) findViewById(R.id.txt_email);
        passwordField = (EditText) findViewById(R.id.txt_password);
        firstNameField = (EditText) findViewById(R.id.txt_first_name);
        lastNameField = (EditText) findViewById(R.id.txt_last_name);
    }

    public void registerNewAccount(View v) {
        FirebaseDBUtil dbUtil = new FirebaseDBUtil();
        String message = "";
        String username = usernameField.getText().toString();
        String password = passwordField.getText().toString();
        String firstName = firstNameField.getText().toString();
        String lastName = lastNameField.getText().toString();

        boolean allFieldsPopulated = true;
        boolean validEmail = isValidEmail(username);
        final User existingUser = new User();
        if(validEmail){
            TaskCompletionSource<User> source = new TaskCompletionSource<>();;
            Task task = source.getTask();
            dbUtil.isEmailExisting(username, source);
            task.addOnCompleteListener(this, new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    User u = (User)task.getResult();
                    if(u != null){
                        existingUser.setId(u.getId());
                        existingUser.setUsername(u.getUsername());
                        existingUser.setPassword(u.getPassword());
                        existingUser.setFirstName(u.getFirstName());
                        existingUser.setLastName(u.getLastName());
                    }
                }
            });
        }
        if((username != null || username != "") && allFieldsPopulated){
            allFieldsPopulated = true;
        } else {
            allFieldsPopulated = false;
            message = "Please enter your username.";
        }
        if((password != null || password != "") && allFieldsPopulated){
            allFieldsPopulated = true;
        } else {
            allFieldsPopulated = false;
            message = "Please enter your password.";
        }
        if((firstName != null || firstName != "") && allFieldsPopulated){
            allFieldsPopulated = true;
        } else {
            allFieldsPopulated = false;
            message = "Please enter your first name.";
        }
        if((lastName != null || lastName != "") && allFieldsPopulated){
            allFieldsPopulated = true;
        } else {
            allFieldsPopulated = false;
            message = "Please enter your last name.";
        }

        if(existingUser.getId() == null) {
            if(allFieldsPopulated) {
                User registeredUser = dbUtil.registerNewAccount(new User(username, password, firstName, lastName));
                if (registeredUser != null) {
                    Intent intent = new Intent(context, HomeActivity.class);
                    intent.putExtra(Constants.USER_ID_LITERAL.toString(), registeredUser.getId());
                    intent.putExtra(Constants.USERNAME_LITERAL.toString(), registeredUser.getUsername());
                    intent.putExtra(Constants.PASSWORD_LITERAL.toString(), registeredUser.getPassword());
                    startActivity(intent);
                    finish();
                    message = "Logged in successfully";
                }
            }
        } else if(!validEmail) {
            message = "Invalid email format.";
        } else {
            message = "Email is already registered.";
        }
        Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();
    }

    private boolean isValidEmail(String email){
        if(TextUtils.isEmpty(email)){
            return false;
        } else {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }
}
