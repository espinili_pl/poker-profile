package profile.poker.com.pokerprofile.listeners;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

import profile.poker.com.pokerprofile.SimpleCallback;
import profile.poker.com.pokerprofile.entities.User;
import profile.poker.com.pokerprofile.utils.Constants;
import profile.poker.com.pokerprofile.utils.EntryPoints;

/**
 * Created by espin on 10/9/2017.
 */

public class DataChangeListener implements ValueEventListener {

    private String username;
    private String password;
    private String entryPoint;
    private SimpleCallback callback;

    public DataChangeListener(){  }

    public DataChangeListener(String entryPoint, SimpleCallback callback){
        this.entryPoint = entryPoint;
        this.callback = callback;
    }
    public DataChangeListener(String username, String password,String entryPoint, SimpleCallback callback){
        this.username = username;
        this.password = password;
        this.callback = callback;
        this.entryPoint = entryPoint;
    }
    public DataChangeListener(String username, String entryPoint, SimpleCallback callback){
        this.username = username;
        this.callback = callback;
        this.entryPoint = entryPoint;
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {

        User returnUser = null;

        if(entryPoint.equals(EntryPoints.LOGIN.toString())) {

            returnUser = getUser(dataSnapshot);

            if(!returnUser.getPassword().equals(password)){
                returnUser = null;
            }
            callback.callback(returnUser);

        } else if(entryPoint.equals(EntryPoints.EMAIL_VALIDATION.toString())){

            returnUser = getUser(dataSnapshot);
            callback.callback(returnUser);

        } else if (entryPoint.equals(EntryPoints.GET_USER.toString())) {

            Map<String, String> userData = (Map<String, String>) dataSnapshot.getValue();

            if(!userData.isEmpty()){
                returnUser = new User();
                returnUser.setUsername(userData.get(Constants.USERNAME_LITERAL.toString()));
                returnUser.setPassword(userData.get(Constants.PASSWORD_LITERAL.toString()));
                returnUser.setFirstName(userData.get(Constants.FIRST_NAME_LITERAL.toString()));
                returnUser.setLastName(userData.get(Constants.LAST_NAME_LITERAL.toString()));
            }
            callback.callback(returnUser);
        }


    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        callback.callback(null);
    }

    private User getUser(DataSnapshot dataSnapshot){
        User returnUser = null;
        Map<String, Object> users = (Map<String, Object>) dataSnapshot.getValue();
        for (Map.Entry<String, Object> obj : users.entrySet()) {
            User user = new User((Map<String, String>) obj.getValue());
            user.setId(obj.getKey());
            if (user.getUsername().equals(username)) {
                returnUser = user;
                break;
            }
        }
        return returnUser;
    }
}
